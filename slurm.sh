#!/bin/bash
#SBATCH -A sua250
#SBATCH --job-name=test
#SBATCH --output=test.txt
#SBATCH -p gpu-debug
#SBATCH --gpus=2
#SBATCH -N 1
#SBATCH --ntasks-per-node=2
#SBATCH --time=00:10:00
#SBATCH --mem=2G
#SBATCH --error=test.err

module purge
module load gpu
module load slurm
module load openmpi
module load cuda
module load anaconda3

prop=example.py
np=2
Job=0
freq=1
Qabs=1e16

Nx=2000
Ny=2000
Nz=1
Mx=25
My=25
Mz=1
Mzslab=1

hx=0.01
hy=0.01
hz=0.01

Npmlx=0
Npmly=10
Npmlz=0

epsbkg=1
epsdiff=1
epstype='vac'
epsfile='c.txt'

Jamp=1.0
Jdir=2
cx=$(($Nx/2))
cy=$(($Ny/2))
cz=0

# solver
maxit=20000
tol=1e-4
verbose=1
init=2
init_type='rand'

solverbase=500

mpiexec -n $np python $prop -Job $Job -freq $freq -Qabs $Qabs -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz -Mzslab $Mzslab -hx $hx -hy $hy -hz $hz -Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz -epsbkg $epsbkg -epsdiff $epsdiff -epstype $epstype -epsfile $epsfile -Jamp $Jamp -Jdir $Jdir -cx $cx -cy $cy -cz $cz -maxit $maxit -tol $tol -verbose $verbose -solverbase $solverbase -init $init -init_type $init_type

import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.tool import write_results, PML, interp, initialize
from lib.solver import Solver
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-Job', action="store", type=int, default=0)
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-4)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
parser.add_argument('-init', action="store", type=int, default=10)
parser.add_argument('-init_type', action="store", type=str, default='zero')
# temporal
parser.add_argument('-freq', action="store", type=float, default=1.0)
parser.add_argument('-Qabs', action="store", type=float, default=1e16)
# geometry
parser.add_argument('-Nx', action="store", type=int, default=200)
parser.add_argument('-Ny', action="store", type=int, default=200)
parser.add_argument('-Nz', action="store", type=int, default=200)

parser.add_argument('-Mx', action="store", type=int, default=50)
parser.add_argument('-My', action="store", type=int, default=50)
parser.add_argument('-Mz', action="store", type=int, default=50)
parser.add_argument('-Mzslab', action="store", type=int, default=1)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

# dielectric
parser.add_argument('-epsbkg', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff', action="store", type=float, default=1.0)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
parser.add_argument('-epstype', action="store", type=str, default='one')
# current source
parser.add_argument('-Jamp', action="store", type=float, default=1.0)
parser.add_argument('-Jdir', action="store", type=int, default=2)
parser.add_argument('-cx', action="store", type=int, default=100)
parser.add_argument('-cy', action="store", type=int, default=100)
parser.add_argument('-cz', action="store", type=int, default=100)

r, unknown = parser.parse_known_args(sys.argv[1:])
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.init=r.init
verbose.init_type=r.init_type
# ----------assembling parameters------------#
if r.Qabs>1e10:
    r.Qabs=float('inf')
    
omega = dtype(r.freq*2*pi) - 1j*dtype(r.freq*2*pi/2/r.Qabs)
shape = (r.Nx,r.Ny,r.Nz)

m=4.0
R=16.0
pmlx=PML(r.Npmlx,omega,r.Nx,r.hx,m,R)
pmly=PML(r.Npmly,omega,r.Ny,r.hy,m,R)
pmlz=PML(r.Npmlz,omega,r.Nz,r.hz,m,R)

pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]
ndof=r.Mx*r.My

# vectors
ep=[None] * 3
j=[None] * 3
x=[None] * 3
b=[None] * 3
if comm.rank == 0:
    print("The size of the problem is ",shape)
    # current source
    j=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)]);
    #j[r.Jdir][r.cx,r.cy,r.cz]=r.Jamp/r.hx/r.hy #electric current
    j[r.Jdir][:,1500,0]=r.Jamp/r.hx/r.hy #electric current

    # epsilon profile
    if r.epstype == 'one':
        dof=np.ones([r.Mx,r.My]).astype(np.float)
    elif r.epstype == 'vac':
        dof=np.zeros([r.Mx,r.My]).astype(np.float)
    elif r.epstype == 'rand':
        dof=np.random.randn(r.Mx,r.My).astype(np.float)
    elif r.epstype == 'file':
        f = open(r.epsfile, 'r')
        dof = np.loadtxt(f).reshape(r.Mx,r.My)

    epsBkg = np.ones(shape)*r.epsbkg
    Mx0= (r.Nx-r.Mx)//2
    My0= (r.Ny-r.My)//2
    Mz0= (r.Nz-r.Mz)//2
    A = interp(r.Mx,r.My,r.Mz,r.Mzslab,r.Nx,r.Ny,r.Nz,Mx0,My0,Mz0)

    ep1 = np.copy(epsBkg)
    A.matA(dof,ep1,r.epsdiff)

    f=h5py.File('epsF.h5','w')
    f.create_dataset('data',data=ep1)
    f.close()

    # isotropic
    ep=dtype([ep1,ep1,ep1])

    # initial solution
    x=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    b=[-1j*omega*np.copy(z) for z in j] #b=-i omega j
    write_results(r.name+'b.h5',b)
    
initialize(x, 0, verbose.init, verbose.init_type, shape, dtype)
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)

if r.Job == 0:
    # test solver
    solver1 = Solver(shape, r.tol, r.maxit, pml_p, pml_d, omega)
    solver1.update_ep(ep)
    if comm.rank==0:
        b=solver1.pre_cond(b)
        write_results(r.name+'b_pre.h5',b)
    # GPU vectors
    xg = [Grid(dtype(f), x_overlap=1) for f in x]
    #-------solve-----------#
    xg, err, success = solver1.Multisolve(b, xg)

    e=[E.get() for E in xg]
    if comm.Get_rank()==0:
        e=solver1.post_cond(e)
        write_results(r.name+'E.h5',e)
        

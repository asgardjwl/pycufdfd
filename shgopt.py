import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.tool import write_results
from lib.tool import PML
from lib.tool import interp
from lib.tool import gradient_check
from lib.ldos import shg_slab
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space
from lib.solver import Solver
from lib.filter import Filter

sys.path.append('/home/weiliang/nlopt-2.5.0/lib/python2.7/site-packages/')
import nlopt

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-6)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
parser.add_argument('-init', action="store", type=int, default=10)
parser.add_argument('-init_type', action="store", type=str, default='zero')
parser.add_argument('-outputfield', action="store", type=int, default=0)
# filter
parser.add_argument('-bproj', action="store", type=float, default=0.0)
parser.add_argument('-R', action="store", type=int, default=0)
parser.add_argument('-alpha', action="store", type=float, default=1.0)
parser.add_argument('-rep', action="store", type=int, default=1)
# Job
parser.add_argument('-Job', action="store", type=int, default=1)
parser.add_argument('-outputbase', action="store", type=int, default=2)
parser.add_argument('-export_in_object', action="store", type=int, default=1)
parser.add_argument('-maxeval', action="store", type=int, default=5000)
# temporal
parser.add_argument('-freq1', action="store", type=float, default=1.0)
parser.add_argument('-freq2', action="store", type=float, default=0.5)
parser.add_argument('-Qabs', action="store", type=float, default=200)
# geometry
parser.add_argument('-Nx', action="store", type=int, default=75)
parser.add_argument('-Ny', action="store", type=int, default=75)
parser.add_argument('-Nz', action="store", type=int, default=70)

parser.add_argument('-Mx', action="store", type=int, default=25)
parser.add_argument('-My', action="store", type=int, default=25)
parser.add_argument('-Mz', action="store", type=int, default=20)
parser.add_argument('-Mx0', action="store", type=int, default=25)
parser.add_argument('-My0', action="store", type=int, default=25)
parser.add_argument('-Mz0', action="store", type=int, default=20)
parser.add_argument('-Mzslab', action="store", type=int, default=1)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

# dielectric
parser.add_argument('-epsbkg1', action="store", type=float, default=1.0)
parser.add_argument('-epssub1', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff1', action="store", type=float, default=11.0)
parser.add_argument('-epsbkg2', action="store", type=float, default=1.0)
parser.add_argument('-epssub2', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff2', action="store", type=float, default=11.0)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
parser.add_argument('-epstype', action="store", type=str, default='vac')

# current source
parser.add_argument('-Jamp', action="store", type=float, default=1.0)
parser.add_argument('-Jdir', action="store", type=int, default=2)
parser.add_argument('-Jopt', action="store", type=int, default=0)
parser.add_argument('-cx', action="store", type=int, default=37)
parser.add_argument('-cy', action="store", type=int, default=37)
parser.add_argument('-cz', action="store", type=int, default=35)

# SHG
parser.add_argument('-p1', action="store", type=int, default=0)
parser.add_argument('-p2', action="store", type=int, default=2)
parser.add_argument('-powerindex', action="store", type=float, default=0.0)

r, unknown = parser.parse_known_args(sys.argv[1:])
if comm.rank==0:
    for arg in vars(r):
        print arg," is ",getattr(r,arg)
        
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.outputbase=r.outputbase
verbose.export_in_object=r.export_in_object
verbose.init=r.init
verbose.init_type=r.init_type
verbose.outputfield=r.outputfield
# ----------assembling parameters------------#
hxyz=r.hx*r.hy*r.hz
if r.Qabs>1e10:
    r.Qabs=float('inf')
    
omega1 = dtype(r.freq1*2*pi) - 1j*dtype(r.freq1*2*pi/2/r.Qabs)
omega2 = dtype(r.freq2*2*pi) - 1j*dtype(r.freq2*2*pi/2/r.Qabs)
shape = (r.Nx,r.Ny,r.Nz)
# pml
pmlx1=PML(r.Npmlx,omega1,r.Nx,r.hx)
pmly1=PML(r.Npmly,omega1,r.Ny,r.hy)
pmlz1=PML(r.Npmlz,omega1,r.Nz,r.hz)

pmlx2=PML(r.Npmlx,omega2,r.Nx,r.hx)
pmly2=PML(r.Npmly,omega2,r.Ny,r.hy)
pmlz2=PML(r.Npmlz,omega2,r.Nz,r.hz)

pml_p1=[pmlx1.sp*r.hx,pmly1.sp*r.hy,pmlz1.sp*r.hz]
pml_d1=[pmlx1.sd*r.hx,pmly1.sd*r.hy,pmlz1.sd*r.hz]

pml_p2=[pmlx2.sp*r.hx,pmly2.sp*r.hy,pmlz2.sp*r.hz]
pml_d2=[pmlx2.sd*r.hx,pmly2.sd*r.hy,pmlz2.sd*r.hz]

#interp matrix
ndof=(r.Mx/r.rep)*(r.My/r.rep)
A = interp(r.Mx,r.My,r.Mz,r.Mzslab,r.Nx,r.Ny,r.Nz,r.Mx0,r.My0,r.Mz0,r.rep)

if r.epstype == 'one':
    dof=np.ones([r.Mx/r.rep,r.My/r.rep]).astype(np.float)
elif r.epstype == 'rand':
    dof=np.random.random(r.Mx/r.rep,r.My/r.rep).astype(np.float)
elif r.epstype == 'file':
    f = open(r.epsfile, 'r')
    dof = np.loadtxt(f).reshape(r.Mx/r.rep,r.My/r.rep)
elif r.epstype == 'vac':
    dof=np.zeros([r.Mx/r.rep,r.My/r.rep]).astype(np.float)
    dof[r.Mx/r.rep/2,r.My/r.rep/2]=1.0

if comm.rank == 0:
    print "The size of the problem is",shape
    # current source
    j=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)]);
    if r.Jopt == 0:
        j[r.Jdir][r.cx,r.cy,r.cz]=r.Jamp/r.hx/r.hy #electric current
    elif r.Jopt == 1:
        jrx = 20
        jry = 20
        j[r.Jdir][r.cx-jrx:r.cx+jrx,r.cy-jry:r.cy+jry,r.cz]=np.random.random((jrx*2,jry*2))*r.Jamp/r.hx/r.hy
    elif r.Jopt == 2:
        jrx = 20
        jry = 20
        j[r.Jdir][r.cx-jrx:r.cx+jrx,r.cy-jry:r.cy+jry,r.cz]=np.ones([jrx*2,jry*2]).astype(np.float)*r.Jamp/r.hx/r.hy
        
    # epsilon profile
    epsBkg1 = np.ones(shape)*r.epsbkg1
    epsBkg1[:,:,:A.Mz0] = r.epssub1

    epsBkg2 = np.ones(shape)*r.epsbkg2
    epsBkg2[:,:,:A.Mz0] = r.epssub2

    ep1 = np.copy(epsBkg1)
    A.matA(dof,ep1,r.epsdiff1)

    ep2 = np.copy(epsBkg2)
    A.matA(dof,ep2,r.epsdiff2)    

    f=h5py.File('epsF1.h5','w')
    f.create_dataset('data',data=ep1)
    f.close()

    f=h5py.File('epsF2.h5','w')
    f.create_dataset('data',data=ep2)
    f.close()    

    # initial solution
    x1=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    x2=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    u1=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    u2=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    u3=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    b=[-1j*omega1*np.copy(z) for z in j] #b=-i omega j
else:
    lap=[None] * 3
    j=[None] * 3
    x1=[None] * 3
    x2=[None] * 3
    u1=[None] * 3
    u2=[None] * 3
    u3=[None] * 3
    b=[None] * 3
    epsBkg1=None
    epsBkg2=None
        
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)
# solver
if r.Job is 0:
    solver1 = Solver(shape, r.tol, r.maxit, pml_p1, pml_d1, omega1)
    solver2 = Solver(shape, r.tol, r.maxit, pml_p2, pml_d2, omega2)
else:
    solver1 = Solver(shape, r.tol, r.maxit, pml_d1, pml_d1, omega1)
    solver2 = Solver(shape, r.tol, r.maxit, pml_d2, pml_d2, omega2)

FT = Filter(r.Mx/r.rep,r.My/r.rep,r.bproj,r.R,r.alpha)

if comm.rank==0:
    b=solver1.pre_cond(b)
#-------solve-----------#
grad=np.zeros(ndof)
shg_old=shg_slab(shape, j, b, x1, x2, u1, u2, u3, solver1, solver2, epsBkg1 ,epsBkg2, r.epsdiff1, r.epsdiff2, A, omega1, omega2, r.p1, r.p2,r.powerindex,hxyz)

shg_nlopt = FT.new_object(shg_old)

if r.Job is 0:
    grad=np.array([])
    val=shg_nlopt(dof.flatten(),grad)
    print val
    
elif r.Job is -1:
    gradient_check(shg_nlopt, dof.flatten(), grad, index=209, Ntotal=20, start=0.0, dx=1e-2)
    
elif r.Job is 1:
    lb=np.ones(ndof)*0.0
    ub=np.ones(ndof)*1.0
    
    opt = nlopt.opt(nlopt.LD_MMA, ndof)
    opt.set_lower_bounds(lb)
    opt.set_upper_bounds(ub)
    
    opt.set_maxeval(r.maxeval)

    opt.set_max_objective(shg_nlopt)
    
    x = opt.optimize(dof.flatten())
    minf = opt.last_optimum_value()


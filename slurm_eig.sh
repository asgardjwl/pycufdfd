#!/bin/bash
#SBATCH --job-name=test
#SBATCH --output=test.txt
#SBATCH -p gpu-shared
#SBATCH --gres=gpu:1
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --time=00:30:00
#SBATCH --mem=10000
#SBATCH --error=test.err

module load cuda openmpi_ib
export PATH="/home/asgard/miniconda3/bin:$PATH"

Job=1
np=1 # number of CPU cores
prop=eig.py
verbose=1

freq=1 # here we assume mu_0=epsilon_0=1

# eigenvalue solver
neig=2 # solve for neig eigenmodes near $freq
Nmax=10 # maximal number of iterations
eigtol=1e-4 # relative tolerance for stopping
qrtol=1e-10 # keep this value small [no need to change]

# Maxwell solver
init_type='zero' # initial guess
maxit=50000 # maximal iterations
tol=1e-5 # relative tolerance for convergence
solverbase=1000 # output information every this number of iterations

# geometries
Nx=400 # pixel number over the entire grid
Ny=400
Nz=1 # for 2D problem, set this to be 1
# grid resolution, so overall domain size is [Nx*hx, Ny*hy,Nz*hz]
hx=0.01
hy=0.01
hz=0.01
# PML pixel points
Npmlx=10
Npmly=10
Npmlz=10

# ---- set up epsilon profiles
# portions of pixels that have differetn epsilon
Mx=20
My=2
Mz=1
Mzslab=1

epsbkg=1 # background
epssub=1 # substrate (along z-direction)
# material epsm = epsr -1j * epsi
epsr=-100
epsi=100
epstype='one' #within the structured region, 'one' for uniform epsm, 'vac' for epsbkg, 'rand' for random, 'file' for reading from file
epsfile='c.m'  # this arguement only works if the above one is set to 'file'

# ------ initial current to excite the mode
Jamp=1.0
Jdir=2 # 0 for x, 1 for y, 2 for z polarization
# position of current
cx=$(($Nx/2))
cy=$(($Ny/2))
cz=$(($Nz/2))
# the size of current, [cx-rx:cx+rx, ...]
rx=5
ry=1
rz=0 # <=0 refer to only one pixel



fpre='mode_f$freq-Nx$Nx-Ny$Ny-Nz$Nz-Mx$Mx-My$My-Mz$Mz-epr$epsr-epi$epsi-n$neig-N$Nmax-tol$eigtol.'
name=`hostname`
ID=$$
outputfile=$name-$fpre.$ID-out

eval "mpiexec -n $np python $prop -name $fpre -Job $Job -freq $freq -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz -Mzslab $Mzslab -hx $hx -hy $hy -hz $hz -Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz -epsbkg $epsbkg -epssub $epssub -epsr $epsr -epsi $epsi -epstype $epstype -epsfile $epsfile -Jamp $Jamp -Jdir $Jdir -cx $cx -cy $cy -cz $cz -rx $rx -ry $ry -rz $rz -maxit $maxit -tol $tol -verbose $verbose -solverbase $solverbase -init_type $init_type -Nmax $Nmax -neig $neig -eigtol $eigtol -qrtol $qrtol >$outputfile"

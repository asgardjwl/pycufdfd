import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.tool import write_results
from lib.tool import PML
from lib.tool import interp
from lib.gce.grid import Grid
from lib.gce.space import initialize_space
from lib.solver import Solver

dtype=np.complex128

name = 'SHG_test'
# solvers
maxit = 100000
tol = 1e-4
verbose = 1
solverbase = 1000 # output information every this number
init_type = 'zero'
init = 10

# temporal information
freq1 = 1.
freq2 = 2.

# --------- geometry -----------#
# pixel size
hx = 0.05
hy = 0.05
hz = 0.05 

# Total computational domain
Lx = 4
Ly = 4
Lz = hz # For a 2D problem in x-y plane, set only one pixel in the z-direction

# Domain for structure
Sx = 2
Sy = 2
Sz = hz

Sx0 = (Lx-Sx)/2 # where the structure begins
Sy0 = (Ly-Sy)/2
Sz0 = (Lz-Sz)/2

# --------- PML information -----------#
Npmlx = 10
Npmly = 10
Npmlz = 10

# --------- dielectric information -----------#
epsbkg1 = 1.
epsdiff1 = 4.
epsbkg2 = 1.
epsdiff2 = 4.
epstype = 'rand'

# --------- Excitation information -----------#
Jamp = 1.
Jdir = 2 # 0,1,2 for x,y,z
cx = Lx/2
cy = Ly/2
cz = Lz/2

# -------------------------------------------#
# ----------assembling parameters------------#
# -------------------------------------------#
Nx = round(Lx/hx)
Ny = round(Ly/hy)
Nz = round(Lz/hz)
Mx = round(Sx/hx)
My = round(Sy/hy)
Mz = round(Sz/hz)
Mx0 = round(Sx0/hx)
My0 = round(Sy0/hy)
Mz0 = round(Sz0/hz)
Mzslab = 1
cx = round(cx/hx)
cy = round(cy/hy)
cz = round(cz/hz)
        
verbose.v=verbose
verbose.solverbase=solverbase
verbose.filename=name
verbose.outputbase=outputbase
verbose.init=init
verbose.init_type=init_type

hxyz=hx*hy*hz
omega1 = dtype(freq1*2*pi)
omega2 = dtype(freq2*2*pi)
shape = (Nx,Ny,Nz)
# pml
pmlx1=PML(Npmlx,omega1,Nx,hx)
pmly1=PML(Npmly,omega1,Ny,hy)
pmlz1=PML(Npmlz,omega1,Nz,hz)

pmlx2=PML(Npmlx,omega2,Nx,hx)
pmly2=PML(Npmly,omega2,Ny,hy)
pmlz2=PML(Npmlz,omega2,Nz,hz)

pml_p1=[pmlx1.sp*hx,pmly1.sp*hy,pmlz1.sp*hz]
pml_d1=[pmlx1.sd*hx,pmly1.sd*hy,pmlz1.sd*hz]

pml_p2=[pmlx2.sp*hx,pmly2.sp*hy,pmlz2.sp*hz]
pml_d2=[pmlx2.sd*hx,pmly2.sd*hy,pmlz2.sd*hz]

#interp matrix
ndof=Mx*My
A = interp(Mx,My,Mz,Mzslab,Nx,Ny,Nz,Mx0,My0,Mz0,1)

if epstype == 'one':
    dof=np.ones([Mx,My]).astype(np.float)
elif epstype == 'rand':
    dof=np.random.random(Mx,My).astype(np.float)
elif epstype == 'vac':
    dof=np.zeros([Mx,My]).astype(np.float)
else:
    f = open(epstype, 'r')
    dof = np.loadtxt(f).reshape(Mx,My)

j=[None] * 3
x1=[None] * 3
x2=[None] * 3
b1=[None] * 3
b2=[None] * 3
if comm.rank == 0:
    print "The size of the problem is",shape
    # current source
    j = [dtype(np.zeros(shape)) for i in range(3)]
    j[Jdir][cx,cy,cz]=Jamp/hx/hy

    # epsilon profile
    epsBkg1 = np.ones(shape)*epsbkg1
    epsBkg2 = np.ones(shape)*epsbkg2

    ep1 = np.copy(epsBkg1)
    A.matA(dof,ep1,epsdiff1)

    ep2 = np.copy(epsBkg2)
    A.matA(dof,ep2,epsdiff2)    

    f=h5py.File('epsF1.h5','w')
    f.create_dataset('data',data=ep1)
    f.close()

    f=h5py.File('epsF2.h5','w')
    f.create_dataset('data',data=ep2)
    f.close()    

    # initial solution
    x1=[dtype(np.zeros(shape)) for i in range(3)]
    x2=[dtype(np.zeros(shape)) for i in range(3)]
    b1=[-1j*omega1*np.copy(z) for z in j] #b=-i omega j
    b2=[dtype(np.zeros(shape)) for i in range(3)]
        
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)
solver1 = Solver(shape, tol, maxit, pml_p1, pml_d1, omega1)
solver2 = Solver(shape, tol, maxit, pml_p2, pml_d2, omega2)

#-------solve-----------#
if comm.rank==0:
    b=solver1.pre_cond(b)



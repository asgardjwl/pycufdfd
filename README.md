FDFD solver with pycuda,  modified from https://github.com/JesseLu/maxwell-solver

To run on expanse: 
module purge
module load gpu
module load openmpi
module load anaconda3
module load cuda

Then, install the following packages with pip (e.g. pip install numpy --user): 
numpy, scipy, mpi4py, pycuda, h5py, psutil, jinja2

Example: sbatch slurm.sh

(The number of CPU cores should equal that of GPU cores)
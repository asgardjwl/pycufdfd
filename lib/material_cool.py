import numpy as np

c0 = 299792458.
eV2freq= 2.417989261097518e+14

class Au:
    def __init__(self,filename='Au.txt'):
        '''
        '''
        tmp = open(filename,'r')
        data = np.loadtxt(tmp)
        self.lam = data[:,0]*1e-6
        self.n = data[:,1]
        self.k = data[:,2]

    def epsilon(self,x,x_type='lambda'):
        xlam = converter2lam(x,x_type)
    
        nout = np.interp(xlam,self.lam,self.n)
        kout = np.interp(xlam,self.lam,self.k)
        ep = (nout - 1j*kout)**2
        return ep

class PET:
    def __init__(self,filename='PET.txt'):
        '''
        '''
        tmp = open(filename,'r')
        data = np.loadtxt(tmp)
        self.lam = data[:,0]*1e-6
        self.n = data[:,1]
        self.k = data[:,2]

    def epsilon(self,x,x_type='lambda'):
        xlam = converter2lam(x,x_type)
    
        nout = np.interp(xlam,self.lam,self.n)
        kout = np.interp(xlam,self.lam,self.k)
        ep = (nout - 1j*kout)**2
        return ep


def converter2lam(x,x_type):
    if x_type == 'lambda':
        return x
    elif x_type == 'freq':
        return c0/x
    elif x_type == 'omega':
        return 2*np.pi*c0/x

def converter2eV(x,x_type):
    if x_type == 'lambda':
        return c0/x/eV2freq
    elif x_type == 'freq':
        return x/eV2freq
    elif x_type == 'omega':
        return x/eV2freq/2/np.pi

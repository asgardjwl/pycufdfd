import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time
from . import mathtool
from math import pi
from .tool import write_results, memory_report, initialize

from .gce.grid import Grid
from .gce import verbose
dtype=np.complex128

def arnoldi_eig(eps, b, solver, M, neig, Nmax, tol, tolqr, solve_init):
    """
    Eigensolver with Arnoldi iteration, where the basis is
    spanned by krylov space {M^-1 b, ..., M^-1^N b}.

    b is not preconditioned
    """
    
    btmp=[None]*3
    xlist = []
    blist = []
    x=[np.zeros(solver.shape),np.zeros(solver.shape),np.zeros(solver.shape)]

    for kn in range(Nmax):
        if comm.rank == 0:
            if  kn == 0:
                btmp = [b[i]*eps[i] for i in range(3)]
            else:
                btmp = [xlist[kn-1][i]*eps[i] for i in range(3)]

            btmp = mathtool.Normalize(btmp,eps)
            blist.append(np.copy(btmp))
            btmp = solver.pre_cond(btmp)
                
        initialize(x, 0, 10, solve_init, solver.shape, dtype)
        xg = [Grid(dtype(f), x_overlap=1) for f in x]
        xg, err, success = solver.Multisolve(btmp, xg)
        xtmp=[E.get() for E in xg]
            
        if comm.rank==0:
            xlist.append(solver.post_cond(xtmp))
        # at leat equal number of neig vectors needed for QR
        if kn<neig-1:
            continue
                
        # QR
        Qn=[]        
        if comm.rank == 0:
            Q,R = mathtool.myqr(xlist,eps,tolqr)
            # assemble H matrix, H=Q^T * blist * inv(R)
            H = dtype(np.zeros_like(R))
            n_actual = len(Q)
            for l in range(n_actual):
                for m in range(n_actual):
                    H[l][m] = mathtool.Inner(Q[l],blist[m])
            H = np.dot(H,np.linalg.inv(R))

            # eigenvale problem
            lam,v=np.linalg.eig(H)
            myv = mathtool.myv(v)
            freqtmp = np.sqrt(lam+solver.omega**2)/2/pi
            freqtmp[np.imag(freqtmp)<0] += 10000j
            #order = np.argsort(abs(lam))
            order = np.argsort(np.imag(freqtmp))

            mylam=lam[order[range(neig)]]
            for k in range(neig):
                Qn.append(mathtool.eigvector(Q,myv[:,order[k]]))
        else:
            for k in range(neig):
                Qn.append([None]*3)
            mylam = 0
            n_actual=0
            
        mylam = comm.bcast(mylam)
        n_actual = comm.bcast(n_actual)
        err = mathtool.all_error(Qn, mylam, M, eps, neig)
        eigfreq = np.sqrt(mylam+solver.omega**2)/2/pi
        quality=np.real(eigfreq)/2.0/np.imag(eigfreq)
        
        if comm.rank == 0:
            print('** At iter ', kn, ', N = ', n_actual, ', errors are ', err)
            for i in range(neig):
                print('     ',i,'-th freq = ', np.real(eigfreq[i]), ' , Q = ',quality[i])

        memory_report()
        #stopping criteria
        if n_actual<kn+1 or max(err) <= tol:
            if comm.rank == 0:
                print('---------- converged ---------- ')
            break

    for i in range(neig):
        if comm.rank == 0:
            print('---- ',i,'-th freq = ', np.real(eigfreq[i]), ' , Q = ',quality[i], ' , error = ',err[i])
        
    return eigfreq, Qn


def krylov_vec(eps, b, solver, Nmax, solve_init):
    """
    only output krylov vectors

    b is not preconditioned
    """
    
    btmp=[None]*3
    x = [None]*3
    if comm.rank == 0:
        x=[np.zeros(solver.shape),np.zeros(solver.shape),np.zeros(solver.shape)]
        initialize(x, 0, 10, solve_init, solver.shape, dtype)

    for kn in range(Nmax):
        if comm.rank == 0:
            if kn == 0:
                btmp = [b[i]*eps[i] for i in range(3)]
            else:
                btmp = [field[i]*eps[i] for i in range(3)]            

            btmp = mathtool.Normalize(btmp,eps)
            btmp = solver.pre_cond(btmp)
                
        xg = [Grid(dtype(f), x_overlap=1) for f in x]
        xg, err, success = solver.Multisolve(btmp, xg)
        
        field=[E.get() for E in xg]
        x[:]=field
            
        if comm.rank==0:
            field=solver.post_cond(field)
            write_results(verbose.filename+'x'+str(kn)+'.h5',field)
            
    memory_report()
    return 1

import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time
from math import pi

from tool import memory_report, write_results, initialize
from gce.grid import Grid
from gce import verbose
dtype=np.complex128

def forceZintegrate(ifreq_list,solver,A,epsBkg,epsdiff,x1,alphaxyz,cx0,cy0,cz0,stypexy=None,stypez=None,Dmethod='center',step=1,Qabs=np.inf,dispersion=-1):
    '''
    if dispersion > 0, fp = dispersion, ep = 1 + fp^2/f^2
    '''
    def nlopt_forceZintegrate(dof,grad):
        # assemble epsilon        
        ep3 = [None]*3
        epD = epsdiff
        if comm.rank==0:
            if A.Mzslab == 1:
                doftmp = dof.reshape(A.Mx/A.rep,A.My/A.rep)
            else:
                doftmp = dof.reshape(A.Mx/A.rep,A.My/A.rep,A.Mz/A.rep)
            ep = dtype(np.copy(epsBkg))
            A.matA(doftmp,ep,epD)
            ep3=[ep,ep,ep]

        hxyz = solver.hxyzL[0]*solver.hxyzL[1]*solver.hxyzL[2]
        grad_cal = False
        if grad.size>0:
            grad_cal = True

        if Dmethod == 'center':
            fw = step
            bw = -step
            Diff = solver.hxyzL[2]*2*step
        elif Dmethod == 'forward':
            fw = step
            bw = 0
            Diff = solver.hxyzL[2]*step
        elif Dmethod == 'backward':
            fw = 0
            bw = -step
            Diff = solver.hxyzL[2]*step
        else:
            raise Exception('Finite Difference methods not defined')
            
        # force
        val_list = []
        if A.Mzslab == 1:
            gradfw=np.zeros((A.Mx/A.rep,A.My/A.rep))
            gradbw=np.zeros((A.Mx/A.rep,A.My/A.rep))
        else:
            gradfw=np.zeros((A.Mx/A.rep,A.My/A.rep,A.Mz/A.rep))
            gradbw=np.zeros((A.Mx/A.rep,A.My/A.rep,A.Mz/A.rep))            
        for ifreq in ifreq_list:
            if dispersion>0:
                epD = (dispersion/ifreq)**2
                if comm.rank == 0:
                    ep = dtype(np.copy(epsBkg))
                    A.matA(doftmp,ep,epD)
                    ep3=[ep,ep,ep]
                    
            omega = -1j*ifreq*2*pi*(1+1j/Qabs)
            v_x = 0.
            v_y = 0.
            v_z = 0.
            # forward parts
            if alphaxyz[0]>0.:
                p = setup_source(solver.shape,cx0,cy0,cz0+fw,0,hxyz,stypexy=stypexy,stypez=stypez)
                valx, gradx = solve_MEE(p,ep3,hxyz,omega,solver,x1,epD,grad=grad_cal)
                v_x= valx*alphaxyz[0]

            if alphaxyz[1]>0.:
                p = setup_source(solver.shape,cx0,cy0,cz0+fw,1,hxyz,stypexy=stypexy,stypez=stypez)
                valy, grady = solve_MEE(p,ep3,hxyz,omega,solver,x1,epD,grad=grad_cal)
                v_y = valy*alphaxyz[1]

            if alphaxyz[2]>0.:
                p = setup_source(solver.shape,cx0,cy0,cz0+fw,2,hxyz,stypexy=stypexy,stypez=stypez)
                valz, gradz = solve_MEE(p,ep3,hxyz,omega,solver,x1,epD,grad=grad_cal)
                v_z = valz*alphaxyz[2]

            if grad.size>0 and comm.rank == 0:
                if alphaxyz[0]>0.:
                    A.matAT(gradfw,gradx,alphaxyz[0])
                if alphaxyz[1]>0.:
                    A.matAT(gradfw,grady,alphaxyz[1])
                if alphaxyz[2]>0.:
                    A.matAT(gradfw,gradz,alphaxyz[2])


            # backward parts
            if alphaxyz[0]>0.:
                p = setup_source(solver.shape,cx0,cy0,cz0+bw,0,hxyz,stypexy=stypexy,stypez=stypez)
                valx, gradx = solve_MEE(p,ep3,hxyz,omega,solver,x1,epD,grad=grad_cal)
                v_x -= valx*alphaxyz[0]

            if alphaxyz[1]>0.:
                p = setup_source(solver.shape,cx0,cy0,cz0+bw,1,hxyz,stypexy=stypexy,stypez=stypez)
                valy, grady = solve_MEE(p,ep3,hxyz,omega,solver,x1,epD,grad=grad_cal)
                v_y -= valy*alphaxyz[1]

            if alphaxyz[2]>0.:
                p = setup_source(solver.shape,cx0,cy0,cz0+bw,2,hxyz,stypexy=stypexy,stypez=stypez)
                valz, gradz = solve_MEE(p,ep3,hxyz,omega,solver,x1,epD,grad=grad_cal)
                v_z -= valz*alphaxyz[2]
                
            if grad.size>0 and comm.rank == 0:
                if alphaxyz[0]>0.:
                    A.matAT(gradbw,gradx,alphaxyz[0])
                if alphaxyz[1]>0.:
                    A.matAT(gradbw,grady,alphaxyz[1])
                if alphaxyz[2]>0.:
                    A.matAT(gradbw,gradz,alphaxyz[2])

            val_list.append(np.array([v_x,v_y,v_z])/Diff)
            if comm.rank==0:
                forcei = 8*pi**2*np.sum(val_list[-1])
                print ">> At ifreq = ",ifreq, ", count ",verbose.count,", chi = ",epD,", force/d^5 = ",forcei

        valfreq = np.sum(np.array(val_list),axis=1)
        if len(ifreq_list)>1:
            force = 8*pi**2*np.trapz(valfreq,x=ifreq_list)
        else:
            force = 8*pi**2*np.sum(valfreq)
        val = np.sum(valfreq)*8*pi**2
        
        if comm.rank==0:
            print "At count ",verbose.count,", force/d^5 = ",force,", val = ",val
            if verbose.export_in_object is 1 and verbose.count%verbose.outputbase is 0:
                np.savetxt(verbose.filename+'dof'+str(verbose.count)+'.txt', dof)

        memory_report(str(verbose.count)+'-')
        if verbose.export_in_object is 1:
            verbose.count +=1

        if grad.size>0:
            if comm.rank == 0:
                grad[:]=(gradfw.flatten()-gradbw.flatten())/Diff*8*pi**2
            grad[:]=comm.bcast(grad)
            return val
        else:
            return val,val_list
    return nlopt_forceZintegrate
    
def setup_source(shape,cx,cy,cz,polarization,hxyz,amp=1.,stypexy=None,stypez=None,cutoff=1e-10):
    '''
    A delta-funcion source, implemented as a pixel source, whose amplitude is 1/hx/hy/hz
    when stype is a number, it's a Gaussion source.
    '''
    p = [None]*3
    if comm.rank == 0:
        if stypexy == None:
            p = [np.zeros(shape,dtype=dtype) for i in range(3)]
            p[polarization][cx,cy,cz] = amp/hxyz
        elif type(stypexy) == float or type(stypexy) == int:
            p = [np.zeros(shape,dtype=dtype) for i in range(3)]

            x,y,z = np.meshgrid(range(shape[0]),range(shape[1]),range(shape[2]),indexing='ij')
            distance = np.exp(-((x-cx)**2+(y-cy)**2)/stypexy**2-(z-cz)**2/stypez**2)/stypexy**2/stypez/pi**1.5
            distance[distance<cutoff] = 0.
            
            p[polarization] = dtype(distance/hxyz)
        else:
            raise Exception('source type not defined')

    return p
                
def solve_MEE(p,ep3,hxyz,omega,solver,x1,epsdiff,grad=True):
    '''
    compute pT (k_0^2 M^-1 p) *hxyz
    '''
    # update solvers
    solver.update_ep_omega(ep3,omega)

    b = [None] * 3
    if comm.rank == 0:
        b = solver.pre_cond(p)
        initialize(x1, verbose.count, verbose.init, verbose.init_type, solver.shape, dtype)
        
    # solve
    xg = [Grid(dtype(f), x_overlap=1) for f in x1]
    xg, err, success = solver.Multisolve(b, xg)
        
    field=[E.get() for E in xg]
    x1[:]=field

    val = 0.
    if comm.rank==0:
        field=solver.post_cond(field)
        for i in range(3):
            val += np.vdot(p[i],field[i])*hxyz**0.5
            
        val=np.real(omega**2*val)
    val = comm.bcast(val)

    if comm.rank==0:
        print "----- ldos = ",val

    # gradient information: Re[ omega_complex^4 E E ]
    if grad == True:
        grad = [None]*3
        if comm.rank == 0:
            grad = [np.real(omega**4*epsdiff*hxyz*field[i]*field[i]) for i in range(3)]

    return val,grad

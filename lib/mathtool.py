import numpy as np
from math import pi
from cmath import sqrt
dtype=np.complex128

from .gce.grid import Grid
from mpi4py.MPI import COMM_WORLD as comm

def Inner(x,y,z=None):
    val=dtype(0.0)
    
    if z is None:
        z=[1.0,1.0,1.0]
    for m in range(3):
        val += np.sum(x[m]*y[m]*z[m])
        
    return val

def Normalize(x,eps):
    norm = sqrt(Inner(x,x,eps))
    xn = [x[i]/norm for i in range(3)]
    return xn

def minus(x,y):
    xn = [x[i]-y[i] for i in range(3)]
    return xn
    
def myqr(xlist,eps,tol):
    n0=len(xlist)
    Q=[]

    for k in range(n0):
        Q.append(xlist[k])
        for l in range(k):
            scale = Inner(Q[k],Q[l],eps)
            ql = [Q[l][i]*scale for i in range(3)]
            Q[k] = minus(Q[k],ql)

        if abs(sqrt(Inner(Q[k],Q[k],eps)))<tol:
            Q.pop()
            break
            
        Q[k] = Normalize(Q[k],eps)
        
    n_actual=len(Q)
    
    # compute R = Q^T * eps * xlist
    R = dtype(np.zeros((n_actual,n_actual)))
    for l in range(n_actual):
        for m in range(n_actual):
            R[l][m] = Inner(Q[l],xlist[m],eps)
        
    return Q,R
            
def eigvector(Q,v):
    n0=len(Q)
    Qn=[np.zeros_like(Q[0][0]),np.zeros_like(Q[0][0]),np.zeros_like(Q[0][0])]
    
    for k in range(n0):
        Qn[0]+=Q[k][0]*v[k]
        Qn[1]+=Q[k][1]*v[k]
        Qn[2]+=Q[k][2]*v[k]

    return Qn
        
def check_err(x,y):
    val = 0.0
    norm = 0.0

    for m in range(3):
        val += np.sum(abs(x[m]-y[m])**2)
        norm += np.sum(abs(y[m])**2+abs(x[m])**2)
    return val/norm*2

def myv(x):
    dim = len(x)
    y = np.zeros_like(x)
    for i in range(dim):
        norm = np.sqrt(np.sum(x[:,i]**2))
        y[:,i]=x[:,i]/norm
    return y    

def all_error(Qn, lam, M, eps, neig):
    err = []
    n = neig
    for k in range(n):
        xm=[Grid(dtype(np.copy(f)), x_overlap=1) for f in Qn[k]]
        v = [Grid(dtype, x_overlap=1) for f in range(3)]
        v = M(xm,v)
        Mx=[E.get() for E in v]

        if comm.rank == 0:
            lepx = [lam[k]*eps[i]*Qn[k][i] for i in range(3)]            
            err.append(check_err(lepx,Mx))
            
    err = comm.bcast(err)
    return err

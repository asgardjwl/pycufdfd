import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
from cmath import pi, sqrt
import cmath
from tool import memory_report
dtype=np.complex128

def rsvd(fun, fun_dagger, tol, Nmax, shape):
    Q, N_actual = getQRfun(fun, tol, Nmax, shape)

    ######## A = Q (Q^\dagger A) = Q F^\dagger
    #   F = (A^\dagger Q)
    F = [fun_dagger(Q[i]) for i in range(N_actual)]
    # F = Q2 R = Q2 u s vh
    Q2,R = getQRvec(F, N_actual, shape)

    ####### A = Q (Q2 u s vh)^\dagger = (Q v) s (Q2 u)^\dagger
    U=[[np.zeros(shape) for k in range(3)] for l in range(N_actual)]
    V=[[np.zeros(shape) for k in range(3)] for l in range(N_actual)]
    s=[]
    if comm.rank == 0:
        u, s, vh = np.linalg.svd(R, full_matrices=True)
        # U = Q vh^\dagger
        for i in range(N_actual):
            for j in range(N_actual):
                U[i] = [U[i][k]+Q[j][k]*np.conj(vh[i][j]) for k in range(3)]
        
        # V = Q2 u
                V[i] = [V[i][k]+Q2[j][k]*u[j][i] for k in range(3)]
                
    s=comm.bcast(s)
    return U, s, V

def rsvd_Vonly(fun, fun_dagger, tol, Nmax, shape):
    Q, N_actual = getQRfun(fun, tol, Nmax, shape)

    ######## A = Q (Q^\dagger A) = Q F^\dagger
    #   F = (A^\dagger Q)
    F=[]
    for i in range(N_actual):
        F.append(fun_dagger(Q[0]))
        del Q[0]
    # F = Q2 R = Q2 u s vh
    Q2,R = getQRvec(F, N_actual, shape)

    ####### A = Q (Q2 u s vh)^\dagger = (Q v) s (Q2 u)^\dagger
    V=[None]*N_actual
    s=[]
    if comm.rank == 0:
        V=[[np.zeros(shape) for k in range(3)] for l in range(N_actual)]
        u, s, vh = np.linalg.svd(R, full_matrices=True)
        for i in range(N_actual):
            for j in range(N_actual):
                # V = Q2 u
                V[i] = [V[i][k]+Q2[j][k]*u[j][i] for k in range(3)]
                
    s=comm.bcast(s)
    return s, V
    
def rsvd_singular(fun, fun_dagger, tol, Nmax, shape):
    Q, N_actual = getQRfun(fun, tol, Nmax, shape)

    ######## A = Q (Q^\dagger A) = Q F^\dagger
    #   F = (A^\dagger Q)
    F=[]
    for i in range(N_actual):
        F.append(fun_dagger(Q[0]))
        del Q[0]
        
    # F = Q2 R = Q2 u s vh
    R = getQRvec_singular(F, N_actual, shape)
    s=[]
    if comm.rank == 0:
        u, s, vh = np.linalg.svd(R, full_matrices=True)
        
    s=comm.bcast(s)
    return s
    
def getQRvec(F, Nmax, shape):
    Q = []
    R = []

    for i in range(Nmax):
        if comm.rank == 0:
            Q.append(F[i])
            for j in range(i):
                overlap = Inner(Q[j],Q[i])
                Q[i] = [Q[i][k] - Q[j][k]*overlap for k in range(3)]
                    
            norm = abs(sqrt(Inner(Q[i],Q[i])))
            Q[i] = [Q[i][k]/norm for k in range(3)]
            
    if comm.rank == 0:
        R = dtype(np.zeros((Nmax,Nmax)))
        for l in range(Nmax):
            for m in range(Nmax):
                R[l][m] = Inner(Q[l],F[m])            
    R = comm.bcast(R)
    return Q, R

def getQRvec_singular(F, Nmax, shape):
    Q = []
    R = []

    for i in range(Nmax):
        if comm.rank == 0:
            Q.append(F[i])
            for j in range(i):
                overlap = Inner(Q[j],Q[i])
                Q[i] = [Q[i][k] - Q[j][k]*overlap for k in range(3)]
                    
            norm = abs(sqrt(Inner(Q[i],Q[i])))
            Q[i] = [Q[i][k]/norm for k in range(3)]

    if comm.rank == 0:
        R = dtype(np.zeros((Nmax,Nmax)))
        for l in range(Nmax):
            for m in range(Nmax):
                R[l][m] = Inner(Q[l],F[m])            
    R = comm.bcast(R)
    return R    
    
def getQRfun(fun, tol, Nmax, shape):
    N_actual = Nmax
    x = [None]*3
    Q = []
    norm=0.0
    normQ=0.0

    for i in range(Nmax):
        if comm.rank == 0:
            #x = [np.random.random(shape)+np.random.random(shape)*1j for k in range(3)]
            x = [np.random.random(shape) for k in range(3)]
        Q.append(fun(x))

        if comm.rank == 0:
            for j in range(i):
                overlap = Inner(Q[j],Q[i])
                Q[i] = [Q[i][k] - Q[j][k]*overlap for k in range(3)]
                    
            norm = abs(sqrt(Inner(Q[i],Q[i])))
            Q[i] = [Q[i][k]/norm for k in range(3)]
            
            if i == 0:
                normQ = norm
            print "!! at step ", i,",  error = ",norm/normQ
            
        memory_report(str(i)+'-')
        norm = comm.bcast(norm)
        normQ = comm.bcast(normQ)
        if norm<tol*normQ:
            N_actual = i + 1
            break
                
    if comm.rank == 0:
        print "!!!!!! converged  error = ",norm/normQ, ", maxsvd = ", Nmax, ", actual num = ", N_actual

    return Q , N_actual

            
def Inner(x,y):
    val=dtype(0.0)
    for m in range(3):
        val += np.vdot(x[m],y[m])
    return val

def vec_conj(x):
    y = [[np.conj(f) for f in m] for m in x]
    return y
    
def vec_product(xv,yv):
    # R = xv^\dagger yv
    R=[]
    m=len(xv)
    n=len(yv)
    R = dtype(np.zeros((m,n)))

    for i in range(m):
        for j in range(n):
            R[i][j] = Inner(xv[i],yv[j])
                
    return R

def svd_mult(U,s,V):
    def fun_mult(x):
        y2 = [None] *3
        if comm.rank == 0:
            y1=[Inner(V[i],x)*s[i] for i in range(len(U))]
            y2=[np.zeros_like(x[0]) for k in range(3)]
            for i in range(len(U)):
                y2=[y2[k]+y1[i]*U[i][k] for k in range(3)]
            
        return y2
    return fun_mult

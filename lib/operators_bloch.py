""" Implements the operations needed to solve Maxwell's equations in 3D. """

import numpy as np
from jinja2 import Environment, PackageLoader, Template
from .gce.space import get_space_info
from .gce.grid import Grid
from .gce.const import Const
from .gce.out import Out
from .gce.kernel import Kernel
from mpi4py.MPI import COMM_WORLD as comm

# Execute when module is loaded.
# Load the jinja environment.
jinja_env = Environment(loader=PackageLoader(__name__, 'kernels'))

def rho_step(dtype):
    """ Return the function to execute the rho step of the bicg algorithm.
        rho_step -- rho_step(alpha, p, r, rh, v, vh, x) updates r, rh, and x, and returns
        rho and the error. Specifically, rho_step performs:
        x = x + alpha * p
        r = r - alpha * v, rh = rh - alpha vh
        rho_(k+1) = (rh dot r)
        err = (conj(r) dot r)    
    """

    # Code for the rho step function.
    code = Template("""
        if (_in_global) {
            x0(0,0,0) = x0(0,0,0) + alpha * p0(0,0,0);
            x1(0,0,0) = x1(0,0,0) + alpha * p1(0,0,0);
            x2(0,0,0) = x2(0,0,0) + alpha * p2(0,0,0);
            r0(0,0,0) = r0(0,0,0) - alpha * v0(0,0,0);
            r1(0,0,0) = r1(0,0,0) - alpha * v1(0,0,0);
            r2(0,0,0) = r2(0,0,0) - alpha * v2(0,0,0);
            rh0(0,0,0) = rh0(0,0,0) - alpha * vh0(0,0,0);
            rh1(0,0,0) = rh1(0,0,0) - alpha * vh1(0,0,0);
            rh2(0,0,0) = rh2(0,0,0) - alpha * vh2(0,0,0);    
            rho += (rh0(0,0,0) * r0(0,0,0)) + (rh1(0,0,0) * r1(0,0,0)) + (rh2(0,0,0) * r2(0,0,0));
            err += conj(r0(0,0,0))*r0(0,0,0) + \
                   conj(r1(0,0,0))*r1(0,0,0) + \
                   conj(r2(0,0,0))*r2(0,0,0);
        } """).render(type=_get_cuda_type(dtype))
    
    # Compile the code.
    grid_names = [A + i for A in ['p', 'r', 'rh', 'v', 'vh', 'x'] for i in ['0', '1', '2']]
    rho_fun = Kernel(code, \
                    ('alpha', 'number', dtype), \
                    ('rho', 'out', dtype), \
                    ('err', 'out', dtype), \
                    *[(name, 'grid', dtype) for name in grid_names], \
                    shape_filter='skinny')

    # Temporary values that are needed.
    rho_out = Out(dtype)
    err_out = Out(dtype)

    # Define the actual function.
    def rho_step(alpha, p, r, rh, v, vh, x):
        rho_fun(dtype(alpha), rho_out, err_out, *(p + r + rh + v + vh + x), \
                post_sync=r+rh) # r must be post-synced for upcoming alpha step.
        return rho_out.get(), np.sqrt(err_out.get())

    return rho_step

def M_step_fun(shape,dtype,transpose=0): 
    """ Define the alpha step function needed for the bicg algorithm.
        M_step -- M_step(rho_k, rho_(k-1), p, r, v) updates p and v.
        Specifically, alpha_step performs:
            p = r + (rho_k / rho_(k-1)) * p
            v = A * p

       if transpose = 0, it will be v = AT *p instead
    """
    num_shared_banks = 6 

    # Render the pre-loop and in-loop code.
    if transpose == 0:
        code_tem = 'M_mult.cu'
    else:
        code_tem = 'MT_mult.cu'
        
    cuda_type = _get_cuda_type(dtype)
    code_allpre = jinja_env.get_template(code_tem).\
                    render(dims=shape, \
                            type=cuda_type)

    # Grid input parameters.
    grid_params = [(A + i, 'grid', dtype) for A in ['P', 'P1', 'R', 'V', 'e', 'm'] \
                                            for i in ['x', 'y', 'z']]

    # Const input parameters.
    const_names = ('sx0', 'sy0', 'sz0', 'sx1', 'sy1', 'sz1') + \
                  ('fx','fy','fz') + ('bx','by','bz')
    const_sizes = shape * 4
    const_params = [(const_names[k], 'const', dtype, const_sizes[k]) \
                        for k in range(len(const_sizes))]

    # Compile.
    M_fun = Kernel('', \
                    ('beta', 'number', dtype), \
                    *(grid_params + const_params), \
                    pre_loop=code_allpre, \
                    padding=(1,1,1,1), \
                    smem_per_thread=num_shared_banks*16, \
                    shape_filter='square')
    return M_fun

def alpha_single_fun(dtype):
    """ Return the function to execute the rho step of the bicg algorithm.
        alpha_step -- alpha_step(rhok, ph, v):
           alpha = rho_k / (ph dot v)
    """

    # Code for the rho step function.
    code = Template("""
        if (_in_global) {
            alpha += (ph0(0,0,0) * v0(0,0,0)) + (ph1(0,0,0) * v1(0,0,0)) + (ph2(0,0,0) * v2(0,0,0));
        } """).render(type=_get_cuda_type(dtype))
    
    # Compile the code.
    grid_names = [A + i for A in ['ph', 'v'] for i in ['0', '1', '2']]
    alpha_fun = Kernel(code, \
                    ('alpha', 'out', dtype), \
                    *[(name, 'grid', dtype) for name in grid_names], \
                    shape_filter='skinny')

    # Temporary values that are needed.
    alpha_out = Out(dtype)

    # Define the actual function.
    def alpha_single_fun(rho_k,ph, v):
        alpha_fun(alpha_out, *(ph + v))
        return rho_k/alpha_out.get()

    return alpha_single_fun
    
def alpha_step(M_fun,MT_fun, alpha_single_fun, shape,pml_p,pml_d,e,m, dtype,bc=None):
    """ Define the alpha step function needed for the bicg algorithm.
        M_step -- M_step(rho_k, rho_(k-1), p, ph, r, rh, v, vh) updates p, ph, v, vh
        Specifically, alpha_step performs:
            p = r + (rho_k / rho_(k-1)) * p, ph = rh + (rho_k / rho_(k-1)) * ph
            v = A * p, vh = AT * ph
    """
    if bc == None:
        back = [np.ones(f,dtype) for f in shape]
        forward = [np.ones(f,dtype) for f in shape]
    else:
        back = bc.back
        forward = bc.forward    
    # Temporary variables.
    p_temp = [Grid(dtype, x_overlap=1) for k in range(3)] # Used to swap p.
    ph_temp = [Grid(dtype, x_overlap=1) for k in range(3)] # Used to swap ph.

    # Grid variables.
    e = [Grid(dtype(f), x_overlap=1) for f in e]
    m = [Grid(dtype(f), x_overlap=1) for f in m]

    # Constant variables.
    sc_pml_0 = [Const(dtype(x**-1)) for x in pml_p]
    sc_pml_1 = [Const(dtype(x**-1)) for x in pml_d]
    back_c = [Const(x) for x in back]
    forward_c = [Const(x) for x in forward]

    # Define the function
    def alpha_step(rho_k, rho_k_1, p, ph, r, rh,  v, vh):
        # Execute cuda code.
        # Notice that p_temp and v are post_synced.
        M_fun(dtype(rho_k/rho_k_1), \
                    *(p + p_temp + r + v + e + m + \
                        sc_pml_0 + sc_pml_1+forward_c+back_c), \
                    post_sync=p_temp+v)
        p[:], p_temp[:] = p_temp[:], p[:] # Deep swap.

        MT_fun(dtype(rho_k/rho_k_1), \
                    *(ph + ph_temp + rh + vh + e + m + \
                        sc_pml_0 + sc_pml_1+forward_c+back_c), \
                    post_sync=ph_temp+vh)
        ph[:], ph_temp[:] = ph_temp[:], ph[:] # Deep swap.

        return alpha_single_fun(rho_k,ph, v)

    return alpha_step

def M_step(M_fun, shape,pml_p,pml_d,e,m, dtype,bc=None):
    """
           set beta=0, p=0, then v = M * r
            p = r + (rho_k / rho_(k-1)) * p,
            v = A * p
    """
    if bc == None:
        back = [np.ones(f,dtype) for f in shape]
        forward = [np.ones(f,dtype) for f in shape]
    else:
        back = bc.back
        forward = bc.forward    
    # Temporary variables.
    p_temp = [Grid(dtype, x_overlap=1) for k in range(3)] # Used to swap p.
    p = [Grid(dtype, x_overlap=1) for k in range(3)]

    # Grid variables.
    e = [Grid(dtype(f), x_overlap=1) for f in e]
    m = [Grid(dtype(f), x_overlap=1) for f in m]

    # Constant variables.
    sc_pml_0 = [Const(dtype(x**-1)) for x in pml_p]
    sc_pml_1 = [Const(dtype(x**-1)) for x in pml_d]
    back_c = [Const(x) for x in back]
    forward_c = [Const(x) for x in forward]

    # Define the function
    def M_step(r,  v):
        # Execute cuda code.
        # Notice that p_temp and v are post_synced.
        M_fun(dtype(0), \
                    *(p + p_temp + r + v + e + m + \
                        sc_pml_0 + sc_pml_1+forward_c+back_c), \
                    post_sync=v)

        return v
    return M_step        

def _get_cuda_type(dtype):
    """ Convert numpy type into cuda type. """
    if dtype is np.complex64:
        return 'pycuda::complex<float>'
    elif dtype is np.complex128:
        return 'pycuda::complex<double>'
    else:
        raise TypeError('Invalid dtype.')

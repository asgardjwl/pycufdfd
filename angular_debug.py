import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from cmath import pi

from lib.tool import write_results, PML, interp, initialize, E2H,Poynting
from lib.solver_bloch import Solver
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-Job', action="store", type=int, default=0)
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-4)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
parser.add_argument('-init', action="store", type=int, default=10)
parser.add_argument('-init_type', action="store", type=str, default='zero')
# temporal
parser.add_argument('-freq', action="store", type=float, default=1.0)
parser.add_argument('-Qabs', action="store", type=float, default=1e16)
# geometry
parser.add_argument('-Nx', action="store", type=int, default=200)
parser.add_argument('-Ny', action="store", type=int, default=200)
parser.add_argument('-Nz', action="store", type=int, default=200)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

parser.add_argument('-theta', action="store", type=float, default=0.0)
parser.add_argument('-phi', action="store", type=float, default=0.0)

# dielectric
parser.add_argument('-epsbkg', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff', action="store", type=float, default=1.0)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
parser.add_argument('-epstype', action="store", type=str, default='one')
# current source
parser.add_argument('-s_amp', action="store", type=float, default=1.)
parser.add_argument('-p_amp', action="store", type=float, default=0.)
parser.add_argument('-cy', action="store", type=int, default=100)

r, unknown = parser.parse_known_args(sys.argv[1:])
if comm.rank == 0:
    for arg in vars(r):
        print(arg," is ",getattr(r,arg))
        
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.init=r.init
verbose.init_type=r.init_type
# ----------assembling parameters------------#
if r.Qabs>1e10:
    r.Qabs=float('inf')
    
omega = dtype(r.freq*2*pi) - 1j*dtype(r.freq*2*pi/2/r.Qabs)
omega_r = r.freq*2*pi
shape = (r.Nx,r.Ny,r.Nz)

theta = r.theta*pi/180.
phi = r.phi*pi/180.
    #k: 
    ##  [  cos0  sin0  0 ] [ 0 ]   [ -ksin0]
    ##  [ -sin0  cos0  0 ] [ -k ] = [ -kcos0]
    ##  [  0     0     1 ] [ 0 ]   [ 0]

    ##  [  cos1  0  sin1 ] [ -k sin0 ]   [ -k sin0cos1]
    ##  [   0    1   0   ] [ -kcos0] =   [ -k cos0]
    ##  [ -sin1  0  cos1 ] [ 0 ]          [ ksin0sin1]
kx = -omega_r*np.sin(theta)*np.cos(phi)
ky = 0.
kz = omega_r*np.sin(theta)*np.sin(phi)

m=4.0
R=16.0
pmlx=PML(r.Npmlx,omega,r.Nx,r.hx,m,R)
pmly=PML(r.Npmly,omega,r.Ny,r.hy,m,R)
pmlz=PML(r.Npmlz,omega,r.Nz,r.hz,m,R)

pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]

# vectors
ep=[None] * 3
j=[None] * 3
x=[None] * 3
b=[None] * 3

cz = 0 # J current
if comm.rank == 0:
    print("The size of the problem is ",shape)
    # current source
    j = [dtype(np.zeros(shape)) for i in range(3)]
    ## At normal incidence, we assume
    ## {0,-k,0} is the k-vector
    ## {e_p, 0, e_s} is the electric field
    ## e_s is the s-polarization complex ampltiude
    ## Now we apply a rotation in the xy-plane by angle[0], then in the xz-plane by angle[1]
    ##  [  cos0  sin0  0 ] [ p ]   [ p cos0]
    ##  [ -sin0  cos0  0 ] [ 0 ] = [ -p sin0]
    ##  [  0     0     1 ] [ s ]   [ s]

    ##  [  cos1  0  sin1 ] [ p cos0 ]   [ p cos0cos1 + s sin1]
    ##  [   0    1   0   ] [ -p sin0] = [ -p sin0]
    ##  [ -sin1  0  cos1 ] [ s ]        [ -p cos0sin1 + s cos1]
   
    #j[0][:,r.cy,cz]=(r.p_amp*np.cos(theta)*np.cos(phi)-r.s_amp*np.sin(phi))/r.hx/r.hy*np.exp(-1j*r.hx*kx*np.linspace(0,r.Nx-1,r.Nx))
    #j[1][:,r.cy,cz]=(r.p_amp*np.sin(theta))/r.hx/r.hy*np.exp(-1j*r.hx*kx*np.linspace(0,r.Nx-1,r.Nx))
    #j[2][:,r.cy,cz]=(r.p_amp*np.cos(theta)*np.sin(phi)+r.s_amp*np.cos(phi))/r.hx/r.hy*np.exp(-1j*r.hx*kx*np.linspace(0,r.Nx-1,r.Nx))

    j[0][:,r.cy,cz]=(r.p_amp*(1+np.cos(theta)**2)*np.cos(phi)+2*r.s_amp*np.sin(phi)*np.cos(theta))/r.hx/r.hy*np.exp(-1j*r.hx*kx*np.linspace(0,r.Nx-1,r.Nx))
    j[2][:,r.cy,cz]=(-r.p_amp*(1+np.cos(theta)**2)*np.sin(phi)+2*r.s_amp*np.cos(phi)*np.cos(theta))/r.hx/r.hy*np.exp(-1j*r.hx*kx*np.linspace(0,r.Nx-1,r.Nx))

    ep1 = np.ones(shape)*(r.epsbkg-0j)

    ep=dtype([ep1,ep1,ep1])

    # initial solution
    x=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    b=[-1j*omega*np.copy(z) for z in j] #b=-i omega j
    
initialize(x, 0, verbose.init, verbose.init_type, shape, dtype)
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)

if r.Job == 0:
    # test solver
    solver1 = Solver(shape, r.tol, r.maxit, pml_p, pml_d, omega,r.hx,r.hy,r.hz,kx=kx,ky=ky,kz=kz)
    solver1.update_ep(ep)
    # GPU vectors
    xg = [Grid(dtype(f), x_overlap=1) for f in x]
    #-------solve-----------#
    xg, err, success = solver1.Multisolve(b, xg)

    e=[E.get() for E in xg]
    
    if comm.rank == 0:
        ep1[:,60:70,0] = 12-1j
        ep=dtype([ep1,ep1,ep1])

    solver1.update_ep(ep)
    # GPU vectors
    xg = [Grid(dtype(f), x_overlap=1) for f in x]
    #-------solve-----------#
    xg, err, success = solver1.Multisolve(b, xg)
    e1=[E.get() for E in xg]

    if comm.Get_rank()==0:
        h = E2H(e,omega,shape,r.hx,r.hy,r.hz,kx=kx,ky=ky,kz=kz)
        P = Poynting(e,h)

        h1 = E2H(e1,omega,shape,r.hx,r.hy,r.hz,kx=kx,ky=ky,kz=kz)
        P1 = Poynting(e1,h1)

        P0 = np.sum(P[1][:,20,0])*r.hx
        Pd = np.sum(P1[1][:,20,0])*r.hx
        Pu = np.sum(P1[1][:,120,0])*r.hx
        print((Pu-Pd)/P0)

        tmp = [np.imag(ep[i])*np.abs(e1[i])**2 for i in range(3)]
        tmpn = np.array([np.sum(f.flatten()) for f in tmp])
        Pa = 0.5*omega_r*np.sum(tmpn.flatten())*r.hx*r.hy
        print(Pa/P0)

        write_results(r.name+'E.h5',e1)
        write_results(r.name+'H.h5',h1)
        write_results(r.name+'P.h5',P1)

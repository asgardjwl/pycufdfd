import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.tool import write_results, PML, interp
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space
from lib.solver import Solver
from lib.operators import M_step
from lib.eigmode import arnoldi_eig, krylov_vec
dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# job
# 0 for krylov vectors only
parser.add_argument('-Job', action="store", type=int, default=1)
parser.add_argument('-shift', action="store", type=int, default=0)
parser.add_argument('-rep', action="store", type=int, default=1)
# solvers
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-6)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
parser.add_argument('-init_type', action="store", type=str, default='zero')
# eig
parser.add_argument('-neig', action="store", type=int, default=1)
parser.add_argument('-Nmax', action="store", type=int, default=10)
parser.add_argument('-eigtol', action="store", type=float, default=1e-3)
parser.add_argument('-qrtol', action="store", type=float, default=1e-10)
# temporal
parser.add_argument('-freq', action="store", type=float, default=1.0)
parser.add_argument('-Qabs', action="store", type=float, default=1e16)
# geometry
parser.add_argument('-Nx', action="store", type=int, default=75)
parser.add_argument('-Ny', action="store", type=int, default=75)
parser.add_argument('-Nz', action="store", type=int, default=70)

parser.add_argument('-Mx', action="store", type=int, default=25)
parser.add_argument('-My', action="store", type=int, default=25)
parser.add_argument('-Mz', action="store", type=int, default=20)
parser.add_argument('-Mzslab', action="store", type=int, default=1)
parser.add_argument('-Mx0', action="store", type=int, default=-1)
parser.add_argument('-My0', action="store", type=int, default=-1)
parser.add_argument('-Mz0', action="store", type=int, default=-1)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-pml_m', action="store", type=float, default=4.0)
parser.add_argument('-pml_R', action="store", type=float, default=16.0)
parser.add_argument('-pml_pd', action="store", type=int, default=1)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

# dielectric
parser.add_argument('-epsbkg', action="store", type=float, default=1.0)
parser.add_argument('-epssub', action="store", type=float, default=1.0)
parser.add_argument('-epsr', action="store", type=float, default=12.0)
parser.add_argument('-epsi', action="store", type=float, default=0.0)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
parser.add_argument('-epstype', action="store", type=str, default='vac')

# current source
parser.add_argument('-Jamp', action="store", type=float, default=1.0)
parser.add_argument('-Jdir', action="store", type=int, default=2)
parser.add_argument('-cx', action="store", type=int, default=37)
parser.add_argument('-cy', action="store", type=int, default=37)
parser.add_argument('-cz', action="store", type=int, default=35)
parser.add_argument('-rx', action="store", type=int, default=2)
parser.add_argument('-ry', action="store", type=int, default=2)
parser.add_argument('-rz', action="store", type=int, default=2)

r, unknown = parser.parse_known_args(sys.argv[1:])
if comm.rank==0:
    for arg in vars(r):
        print(arg," is ",getattr(r,arg))
        
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.init_type=r.init_type
# ----------assembling parameters------------#
hxyz=r.hx*r.hy*r.hz
if r.Qabs>1e10:
    r.Qabs=float('inf')
    
omega = dtype(r.freq*2*pi) - 1j*dtype(r.freq*2*pi/2/r.Qabs)
shape = (r.Nx,r.Ny,r.Nz)
# pml
pmlx=PML(r.Npmlx,omega,r.Nx,r.hx,r.pml_m,r.pml_R)
pmly=PML(r.Npmly,omega,r.Ny,r.hy,r.pml_m,r.pml_R)
pmlz=PML(r.Npmlz,omega,r.Nz,r.hz,r.pml_m,r.pml_R)

pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]

#interp matrix
ndof=(r.Mx//r.rep)*(r.My//r.rep)
if r.Mx0<0:
    Mx0= (r.Nx-r.Mx)//2
else:
    Mx0=r.Mx0
if r.My0<0:
    My0= (r.Ny-r.My)//2
else:
    My0=r.My0
if r.Mz0<0:
    Mz0= (r.Nz-r.Mz)//2
else:
    Mz0=r.Mz0
    
A = interp(r.Mx,r.My,r.Mz,r.Mzslab,r.Nx,r.Ny,r.Nz,Mx0,My0,Mz0,r.rep)

if r.epstype == 'one':
    dof=np.ones([r.Mx//r.rep,r.My//r.rep]).astype(np.float)
elif r.epstype == 'rand':
    dof=np.random.random(r.Mx//r.rep,r.My//r.rep).astype(np.float)
elif r.epstype == 'file':
    f = open(r.epsfile, 'r')
    dof = np.loadtxt(f).reshape(r.Mx//r.rep,r.My//r.rep)
elif r.epstype == 'vac':
    dof=np.zeros([r.Mx//r.rep,r.My//r.rep]).astype(np.float)

if comm.rank == 0:
    print("The size of the problem is",shape)
    # current source
    j=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)]);
    if r.rz>0:
        j[r.Jdir][r.cx-r.rx:r.cx+r.rx,r.cy-r.ry:r.cy+r.ry,r.cz-r.rz:r.cz+r.rz]=np.random.rand(2*r.rx,2*r.ry,2*r.rz)
    else:
        j[r.Jdir][r.cx-r.rx:r.cx+r.rx,r.cy-r.ry:r.cy+r.ry,r.cz]=np.random.rand(2*r.rx,2*r.ry)

    # epsilon profile
    epsBkg = np.ones(shape,dtype=dtype)*r.epsbkg
    epsBkg[:,:,:A.Mz0] = r.epssub

    ep = np.copy(epsBkg)
    A.matA(dof,ep,r.epsr-1-1j*r.epsi)

    f=h5py.File(r.name+'epsFr.h5','w')
    f.create_dataset('data',data=np.real(ep))
    f.close()

    f=h5py.File(r.name+'epsFi.h5','w')
    f.create_dataset('data',data=np.imag(ep))
    f.close()    

    eps=dtype([ep,ep,ep])
    if r.shift == 1:
        eps[0][0:-1,:,:]=(eps[0][0:-1,:,:]+eps[0][1:,:,:])/2
        eps[1][:,0:-1,:]=(eps[1][:,0:-1,:]+eps[1][:,1:,:])/2
        eps[2][:,:,0:-1]=(eps[2][:,:,0:-1]+eps[2][:,:,1:])/2

    # initial solution
    x=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    b=[-1j*omega*np.copy(z) for z in j] #b=-i omega j
        
else:
    j=[None] * 3
    x=[None] * 3
    b=[None] * 3
    epsBkg=None
    ep=[None]
    eps=[None] * 3
        
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)
if r.pml_pd == 1:
    solver = Solver(shape, r.tol, r.maxit, pml_p, pml_d, omega)
else:
    solver = Solver(shape, r.tol, r.maxit, pml_d, pml_d, omega)
    
solver.update_ep(eps)

epw2 = [None]*3
muinv=[None]*3
if comm.rank==0:
    muinv = [np.ones(shape),np.ones(shape),np.ones(shape)]
    epw2 = [omega**2 * f for f in eps]
M = M_step(shape,pml_p,pml_d,epw2,muinv,dtype)

# eigensolver
if r.Job == 1:
    eigfreq, Qn = arnoldi_eig(eps, b, solver, M, r.neig, r.Nmax, r.eigtol, r.qrtol, r.init_type)
    for i in range(r.neig):
        if comm.rank == 0:
            write_results(r.name+'x'+str(i)+'.h5',Qn[i])
elif r.Job == 0:
    krylov_vec(eps, b, solver, r.Nmax, r.init_type)

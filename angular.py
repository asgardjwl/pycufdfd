import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from cmath import pi

from lib.tool import write_results, PML, interp, initialize, E2H,Poynting
from lib.solver_bloch import Solver
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.material_cool import Au, PET
from lib.gce.space import initialize_space

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-4)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
parser.add_argument('-init', action="store", type=int, default=10)
parser.add_argument('-init_type', action="store", type=str, default='zero')
# freq,theta,phi
     # the file should have 3 columns: freq, theta, phi
parser.add_argument('-readfile', action="store", type=str, default='freqangle')
parser.add_argument('-lam0', action="store", type=float, default=1.)
# geometry
parser.add_argument('-Nx', action="store", type=int, default=200)
parser.add_argument('-Ny', action="store", type=int, default=200)
parser.add_argument('-Nz', action="store", type=int, default=200)

parser.add_argument('-Nsub', action="store", type=int, default=10)
parser.add_argument('-Nh', action="store", type=int, default=10)
parser.add_argument('-Nthick', action="store", type=int, default=10)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

# current source
parser.add_argument('-s_amp', action="store", type=float, default=1.)
parser.add_argument('-p_amp', action="store", type=float, default=0.)
parser.add_argument('-cy', action="store", type=int, default=100)
parser.add_argument('-cyprobe', action="store", type=int, default=100)

r, unknown = parser.parse_known_args(sys.argv[1:])
if comm.rank == 0:
    for arg in vars(r):
        print(arg," is ",getattr(r,arg))
        
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.init=r.init
verbose.init_type=r.init_type


# ----------assembling parameters------------#
shape = (r.Nx,r.Ny,r.Nz)
m=4.0
R=16.0

# materials
Au = Au()
PET = PET()

# -------- geometry ---------#
x,y,z = np.mgrid[0:shape[0],0:shape[1],0:shape[2]]
# PET regime
dofPET = np.ones(shape)
leftup = y-2.*r.Nh/r.Nx*x-r.Nsub>0
rightup = y+2.*r.Nh/r.Nx*x-2*r.Nh-r.Nsub>0
dofPET[leftup] = 0
dofPET[rightup] = 0

# Au regime
dofAu = np.ones(shape)
leftup = y-2.*r.Nh/r.Nx*x-r.Nsub>0
rightdown = y+2.*r.Nh/r.Nx*x-2*r.Nh-r.Nsub<=0
rightup = y-r.Nsub+2.*r.Nh/r.Nx*(x-r.Nx-r.Nthick/r.Nh*np.sqrt(r.Nh**2+.25*r.Nx**2))>0
dofAu[leftup] = 0
dofAu[rightup] = 0
dofAu[rightdown] = 0

f = open(r.readfile, 'r')
tmp = np.loadtxt(f)
if len(tmp.shape)<2:
    tmp = tmp.reshape(1,3)
freq0 = tmp[:,0]
theta0 = tmp[:,1]*pi/180.
phi0 = tmp[:,2]*pi/180.
N = len(freq0)

#initiliaze GPU
initialize_space(shape)
if comm.rank == 0:
    print("The size of the problem is ",shape)
    print("The number of freq-k is ",N)

cz = 0 # J current
lam0 = r.lam0*1e-6
for i in range(N):
    freq = freq0[i]
    theta = theta0[i]
    phi = phi0[i]

    ep_PET = PET.epsilon(lam0/freq)
    ep_Au = Au.epsilon(lam0/freq)

    omega = dtype(freq*2*pi)
    kx = -omega*np.sin(theta)*np.cos(phi)
    ky = 0.
    kz = omega*np.sin(theta)*np.sin(phi)

    pmlx=PML(r.Npmlx,omega,r.Nx,r.hx,m,R)
    pmly=PML(r.Npmly,omega,r.Ny,r.hy,m,R)
    pmlz=PML(r.Npmlz,omega,r.Nz,r.hz,m,R)

    pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
    pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]

    # vectors
    ep=[None] * 3
    j=[None] * 3
    x=[None] * 3
    b=[None] * 3
    if comm.rank == 0:
        # current source
        j = [dtype(np.zeros(shape)) for i in range(3)]

        j[0][:,r.cy,cz]=(r.p_amp*(1+np.cos(theta)**2)*np.cos(phi)+2*r.s_amp*np.sin(phi)*np.cos(theta))/r.hx/r.hy*np.exp(-1j*r.hx*kx*np.linspace(0,r.Nx-1,r.Nx))
        j[2][:,r.cy,cz]=(-r.p_amp*(1+np.cos(theta)**2)*np.sin(phi)+2*r.s_amp*np.cos(phi)*np.cos(theta))/r.hx/r.hy*np.exp(-1j*r.hx*kx*np.linspace(0,r.Nx-1,r.Nx))

        epbkg = np.ones(shape,dtype=dtype)
        epall = epbkg + dofPET * (ep_PET-1) + dofAu * (ep_Au - 1)
        # f=h5py.File('epsF.h5','w')
        # f.create_dataset('data',data=np.real(epall))
        # f.close()
        ep = [epall for i in range(3)]

        # initial solution
        x = [np.zeros(shape,dtype=dtype) for i in range(3)]
        b=[-1j*omega*np.copy(z) for z in j] #b=-i omega j
    
    initialize(x, 0, verbose.init, verbose.init_type, shape, dtype)
    solver1 = Solver(shape, r.tol, r.maxit, pml_p, pml_d, omega,r.hx,r.hy,r.hz,kx=kx,ky=ky,kz=kz)

    # vacuum
    xg = [Grid(dtype(f), x_overlap=1) for f in x]
    #-------solve-----------#
    xg, err, success = solver1.Multisolve(b, xg)
    evac=[E.get() for E in xg]

    # with structure
    solver1.update_ep(ep)
    # GPU vectors
    xg = [Grid(dtype(f), x_overlap=1) for f in x]
    #-------solve-----------#
    xg, err, success = solver1.Multisolve(b, xg)
    e=[E.get() for E in xg]

    if comm.rank == 0:
        hvac = E2H(evac,omega,shape,r.hx,r.hy,r.hz,kx=kx,ky=ky,kz=kz)
        P = Poynting(evac,hvac)

        Pvac = np.sum(P[1][:,r.cyprobe,0])*r.hx

        tmp = [np.imag(ep[i])*np.abs(e[i])**2 for i in range(3)]
        tmpn = np.array([np.sum(f.flatten()) for f in tmp])
        Pabs = 0.5*omega*np.sum(tmpn.flatten())*r.hx*r.hy
        print(np.real(Pabs)/Pvac)
